import argparse
from collections import deque
import more_itertools
import os
import statistics
from PIL import Image, ImageDraw
import struct
import subprocess
from tqdm import tqdm
from typing import List
import wave

EPSILON = 1e-10

NUM_AMIGA_CHANNELS = 4

SAMPLES_PER_FRAME = None
PIXELS_PER_SAMPLE = 2

MARGIN = 8
VERTICAL_DIM = 720 // NUM_AMIGA_CHANNELS - MARGIN


class Normalisator:
    def __init__(self, normalisation_length: int):
        assert normalisation_length >= 0
        self.normalisation_length = normalisation_length
        if self.normalisation_length == 0:
            self.normalisers = deque([1.0])
        else:
            self.normalisers = deque([1.0], maxlen=self.normalisation_length)

    def add_normaliser(self, normaliser: float):
        if self.normalisation_length > 0:
            self.normalisers.append(normaliser)
        return min(self.normalisers)


class FrameImage:
    def __init__(self):
        self.im = Image.new('RGB',
                            (SAMPLES_PER_FRAME * PIXELS_PER_SAMPLE,
                             (VERTICAL_DIM + MARGIN) * NUM_AMIGA_CHANNELS))
        self.px = self.im.load()  # For drawing pixels
        self.im_line = ImageDraw.Draw(self.im)  # For drawing lines


def _plot_channel(fi: FrameImage, channel: int, signal: List[float]):
    base_y = channel * (VERTICAL_DIM + MARGIN) + VERTICAL_DIM // 2

    for x in range(len(signal)):
        y = base_y + int(signal[x] * (VERTICAL_DIM // 2 - 1))

        if (x + 1) < len(signal):
            next_y = base_y + int(signal[x + 1] * (VERTICAL_DIM // 2 - 1))

            shape = [(PIXELS_PER_SAMPLE * x, y),
                     (PIXELS_PER_SAMPLE * (x + 1), next_y)]
            fi.im_line.line(shape)
        else:
            fi.px[PIXELS_PER_SAMPLE * x, y] = (255, 255, 255)


def _plot_samples(normalisator: Normalisator, samples: List[float]):
    abs_max = max(EPSILON, max([abs(x) for x in samples]))
    normaliser = normalisator.add_normaliser(1.0 / abs_max)

    fi = FrameImage()

    signals = list(more_itertools.chunked(samples, SAMPLES_PER_FRAME))
    assert len(signals) == NUM_AMIGA_CHANNELS

    for channel, signal in enumerate(signals):
        assert len(signal) == SAMPLES_PER_FRAME
        normalised_signal = [normaliser * x for x in signal]
        _plot_channel(fi, channel, normalised_signal)

    return fi.im


def _read_value(proc, struct_type, value_name):
    try:
        b = proc.stdout.read(struct.calcsize(struct_type))
    except struct.error as e:
        print('Error reading {}: {}'.format(value_name, e))
        proc.kill()
        proc.communicate()
        proc.wait()
        return None
    return struct.unpack(struct_type, b)[0]


def main(main_args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('file', nargs=1,
                        help='Register dump file (.reg) produced by uadecode.')
    parser.add_argument('--accelerator', required=True)
    parser.add_argument('--batch', action='store_true')
    parser.add_argument('--fps', type=int, default=60, help='Set framerate')
    parser.add_argument('--image-prefix', default='output_')
    parser.add_argument('--image-format', default='png')
    parser.add_argument('--manual', action='store_true')
    parser.add_argument('--normalisation-length', type=int, default=50)
    parser.add_argument('--sampling-rate', default=44100)
    parser.add_argument('--target-dir', required=True)
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--wave', required=True)
    args = parser.parse_args(args=main_args)

    assert os.path.isdir(args.target_dir)

    proc = subprocess.Popen([args.accelerator, '--wave', args.wave,
                             '--fps', str(args.fps), args.file[0]],
                            stdout=subprocess.PIPE)

    fps = _read_value(proc, 'i', 'fps')
    if fps is None:
        return 1
    if fps <= 0:
        raise ValueError('fps value is non-positive')

    global SAMPLES_PER_FRAME
    SAMPLES_PER_FRAME = _read_value(proc, 'N', 'SAMPLES_PER_FRAME')
    if SAMPLES_PER_FRAME is None:
        return 1
    assert (SAMPLES_PER_FRAME * PIXELS_PER_SAMPLE) == 1280

    num_frames = _read_value(proc, 'N', 'num_frames')
    if num_frames is None:
        return 1
    progress_bar = None
    if num_frames > 0:
        progress_bar = tqdm(total=num_frames, disable=args.batch)

    num_images = 0

    normalisator = Normalisator(args.normalisation_length)

    SAMPLES_PER_IMAGE = NUM_AMIGA_CHANNELS * SAMPLES_PER_FRAME
    BYTES_PER_IMAGE = SAMPLES_PER_IMAGE * struct.calcsize('f')

    while True:
        # See src/write_audio.c: struct uade_write_audio_frame. It describes
        # the data format of the frame.
        unpacked_list = struct.iter_unpack('f',
                                           proc.stdout.read(BYTES_PER_IMAGE))
        samples = [f[0] for f in unpacked_list]
        if len(samples) == 0:
            break
        assert len(samples) == SAMPLES_PER_IMAGE

        im = _plot_samples(normalisator, samples)
        if args.manual:
            print('image frame', num_images)
            im.show()
            input('Enter to continue...')

        basename = '{}{:06d}.{}'.format(
            args.image_prefix, num_images, args.image_format)
        image_path = os.path.join(args.target_dir, basename)
        im.save(image_path, args.image_format, compress_level=1)

        num_images += 1

        if num_frames > 0:
            progress_bar.update(1)

    proc.communicate()

    if proc.wait() != 0:
        print('write-audio binary returned: {}'.format(proc.returncode))
        return 1

    return 0


if __name__ == '__main__':
    main()
